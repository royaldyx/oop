<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

// Kambing
// echo "<h4>Animal : Sheep</h4> <br>";
$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "<br>";// "shaun"
echo "Legs : "  . $sheep->legs . "<br>"; // 4
echo "Cold blooded : " .  $sheep->cold_blooded . "<br><br>"; // "no"
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

// Katak
// echo "<h4>Animal : Frog</h4>";
$frog = new Frog("buduk");
echo "Name : " . $frog->name . "<br>";// "buduk"
echo "Legs : "  . $frog->legs . "<br>"; // 4
echo "Cold blooded : " .  $frog->cold_blooded . "<br>"; // "no"
echo $frog->Jump() . "<br>";

// Kera
// echo "<h4>Animal : Ape</h4>";
$ape = new Ape("kera sakti");
echo "Nama : " . $ape->name . "<br>";// "kera sakti"
echo "Legs : "  . $ape->legs . "<br>"; // 2
echo "Cold blooded : " .  $ape->cold_blooded . "<br>"; // "no"
echo $ape->Yell() . "<br>";






?>